#!/bin/sh
# chkconfig: 345 86 14
# description: Startup and shutdown script for Redis
REDIS_DIR=/web/redis/
REDIS_CONF=/etc/redis.conf
REDIS_PID=/var/run/redis.pid
 
case $1 in
    'start' )
        if test -x $REDIS_DIR/bin/redis-server
        then
            echo "Starting Redis..."
            if $REDIS_DIR/bin/redis-server $REDIS_CONF
            then
            echo "OK"
            else
                echo "failed"
            fi
    else
        echo "Couldn't find Redis Server ($REDIS_DIR/bin/redis-server)"
    fi
        ;;
 
    'stop' )
        echo "Stopping Redis..."
        kill `cat $REDIS_PID`
        ;;
 
    'restart'|'reload' )
        ${0} stop
        ${0} start
        ;;
 
    'list' )
        ps aux | egrep '(PID|redis-server)'
        ;;
    *)
echo "usage: `basename $0` {start|restart|reload|stop|list}"